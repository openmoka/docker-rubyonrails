FROM ruby:2.3.3

# Install components
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
# Create the backend app directory
RUN mkdir /gem
WORKDIR /gem
ADD Gemfile /gem/Gemfile
ADD Gemfile.lock /gem/Gemfile.lock
RUN gem install minitest -v '5.9.1'
# RUN bundle update mongo
RUN bundle install

WORKDIR /

